import os, sys
from nault import sign, get_address, auth, process_block
from cipher import encrypt, decrypt
import customtkinter as ctk
from PIL import Image, ImageTk
import tkinter
from functools import partial


ctk.set_appearance_mode('dark')
ctk.set_default_color_theme('dark-blue')

custom_font = ('helvetica', 16)

class App(ctk.CTk):

    frames = {}
    current = None
    bg = ""


    def __init__(self):
        super().__init__()
        self.bg = self.cget("fg_color")
        self.num_of_frames = 0
        # self.state('withdraw')
        self.title("Change Frames")

        # screen size
        self.geometry("700x420")

        # root!
        main_container = ctk.CTkFrame(self, corner_radius=8, fg_color=self.bg)
        main_container.pack(fill=tkinter.BOTH, expand=True, padx=8, pady=8)

        # left side panel -> for frame selection
        self.left_side_panel = ctk.CTkFrame(main_container, width=280, corner_radius=8, fg_color=self.bg)
        self.left_side_panel.pack(side=tkinter.LEFT, fill=tkinter.Y, expand=False, padx=18, pady=10)

        # right side panel -> to show the frame1 or frame 2, or ... frame + n where n <= 5
        self.right_side_panel = ctk.CTkFrame(main_container, corner_radius=8, fg_color="#212121")
        self.right_side_panel.pack(side=tkinter.LEFT, fill=tkinter.BOTH, expand=True, padx=0, pady=0)
        self.right_side_panel.configure(border_width = 1)
        self.right_side_panel.configure(border_color = "#323232")
        self.create_nav( self.left_side_panel, "frame1", self.color_by_id(1)[0])
        self.create_nav( self.left_side_panel, "frame2", self.color_by_id(2)[0])

    def color_by_id(self, id):
        if id == 1: 
            return ["gray15", 'Remote Signing']
        
        elif id == 2:
            return ["gray15", 'Private Key']

    # button to select the correct frame
    def frame_selector_bt(self, parent, frame_id):
        # create frame 
        bt_frame = ctk.CTkButton(parent)
        # style frame
        bt_frame.configure(height = 40)
        # creates a text label
        bt_frame.configure(text = self.color_by_id(self.num_of_frames + 1)[1]  , font=custom_font, width=200)
        bt_frame.configure(command =  partial( self.toggle_frame_by_id,  "frame" + str(self.num_of_frames + 1) ) )
        # set layout
        bt_frame.grid(pady = 10, row=self.num_of_frames, column=0)
        # update state
        self.num_of_frames = self.num_of_frames + 1

    # create the frame
    def create_frame(self, frame_id, color):
        App.frames[frame_id] = ctk.CTkFrame(self, fg_color=self.cget("fg_color"))
        App.frames[frame_id].configure(corner_radius = 8)
        App.frames[frame_id].configure(fg_color = color)
        App.frames[frame_id].configure(border_width = 2)
        App.frames[frame_id].configure(border_color = "#323232")
        App.frames[frame_id].padx = 8


    # method to change frames
    def toggle_frame_by_id(self, frame_id):
        
        if App.frames[frame_id] is not None:
            if App.current is App.frames[frame_id]:
                App.current.pack_forget()
                App.current = None
            elif App.current is not None:
                App.current.pack_forget()
                App.current = App.frames[frame_id]
                App.current.pack(in_=self.right_side_panel, side=tkinter.TOP, fill=tkinter.BOTH, expand=True, padx=0, pady=0)
            else:
                App.current = App.frames[frame_id]
                App.current.pack(in_=self.right_side_panel, side=tkinter.TOP, fill=tkinter.BOTH, expand=True, padx=0, pady=0)

    # method to create a pair button selector and its related frame
    def create_nav(self, parent, frame_id, frame_color):
        self.frame_selector_bt(parent, frame_id) 
        self.create_frame(frame_id, frame_color)

def get_ad():
    get_address()

def sign_block():
    nn.configure(text='')
    block = block_entry.get()
    pss = pass_entry.get()
    is_signed, message = sign(block=block, password=pss)

    block_entry.delete(0, 'end')
    pass_entry.delete(0, 'end')

    if is_signed: nn.configure(text=message, text_color='green', font=custom_font)
    if not is_signed: nn.configure(text=message, text_color='red',font=custom_font)

a = App()

# ON MODIRY ENTRY
def callback(sv):
    can_process, block, main_block=process_block(block=block_entry.get())

    if can_process: nn.configure(text='valid block', text_color='green', font=custom_font)
    else: nn.configure(text=block, text_color='red', font=custom_font)

# NEW PRIVATE KEY
def new_priv():
     new_priv=priv_key_entry.get()
     new_priv_pass = priv_pass_entry.get()
     enc_confirm=encrypt(password=new_priv_pass,text=new_priv)
     priv_key_entry.delete(0, 'end')
     priv_pass_entry.delete(0, 'end')
     print(enc_confirm)
     mm = ctk.CTkLabel(a.frames['frame2'], text=f'\n\n\n\n\n{enc_confirm}', text_color='green')
     mm.pack()
     a.frames['frame2'].after(5000, lambda: mm.destroy())

# ADDRESS QR
qr_orig = Image.open(os.path.join(sys._MEIPASS, 'qr.png')).resize((270,270))
#qr_orig = Image.open('qr.png').resize((270,270))
qr_tk = ImageTk.PhotoImage(qr_orig)
qr = tkinter.ttk.Button(a.left_side_panel, text='', image=qr_tk, command=get_ad)
qr.grid(pady=100, row=3, column=0)

# FRAME 0
note = '''
Offline signing of blocks for NANO Currency
Add a private key to start signing blocks.
If you think your password is compromised, 
clear the private key immediately.'''
note_label = ctk.CTkLabel(master=a.right_side_panel, text=note, text_color='yellow', font=('helvetica', 16, 'italic'))
note_label.place(x=70, y=110)

# FRAME1
n = ctk.CTkLabel(a.frames['frame1'], text='\n\n\n\n')
n.pack()
nn = ctk.CTkLabel(a.frames['frame1'], text='')
nn.pack()
block_sv = ctk.StringVar()
block_sv.trace('w', lambda name, index, mode, sv=block_sv: callback(sv))
block_entry = ctk.CTkEntry(a.frames['frame1'], width=300, justify='center', placeholder_text='paste block here', font=custom_font, textvariable=block_sv)
block_entry.place(x=73, y=110)
pass_entry = ctk.CTkEntry(a.frames['frame1'], width=300, justify='center', placeholder_text='password', show='*', font=custom_font)
pass_entry.place(x=73, y=150)
sign_btn = ctk.CTkButton(a.frames['frame1'], width=300, text='Sign', command=sign_block, font=custom_font)
sign_btn.place(x=73, y=190)

# FRAME2
priv_key_entry = ctk.CTkEntry(a.frames['frame2'], width=300, placeholder_text='input private key', font=custom_font)
priv_key_entry.place(x=73, y=110)
priv_pass_entry= ctk.CTkEntry(a.frames['frame2'], width=300, placeholder_text='input password', font=custom_font, show='*')
priv_pass_entry.place(x=73, y=150)

new_priv_bt=ctk.CTkButton(a.frames['frame2'], text='New Private Key', width=300, font=custom_font, command=new_priv)
new_priv_bt.place(x=73, y=190)

a.resizable(False, False)
a.mainloop()

