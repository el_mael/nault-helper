# nault-helper
for use with nault.cc or nault.pro for offline signing without using the browser
## Requires
nanopy
```
pip install nanopy
```
cryptography
```
pip install cryptography
```
## Getting files ready
- run `encryptor.py` first to get started
- add `qr.png` file for address in the root folder

## additional notes
- run `main.py` to start
- to pack with `pyinstaller`, be sure to add `nault.py`. `cipher.py` and `.txt` files to root. `--add-data "*.txt;."`, `--add-data "*.py;."` , `--add-data "qr.png;."`. Change all lines that point to these files as  indicated with #.
