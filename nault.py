from nanopy import block_create as bc 
from cipher import decrypt, set_cipher
import json, re
import pyperclip as pc
import os, sys

def process_block(block):
	try:
		block = re.findall('({[\\S\\s]+})', block.strip())[0]
		main_block = json.loads(block)
		block = main_block['block']
		return True, block, main_block
	except:
		return False, 'unable to process block', block

def sign(block, password):
	main_block = block
	can_process, block, main_block = process_block(block=main_block)

	try:
		key = decrypt(password)
		for_sign = bc(key, block['previous'], block['representative'], block['balance'], block['link'])
		main_block['block']['signature'] = for_sign['signature'] 
	except:
		return False, 'wrong password' 
	
	standard_block = str(main_block).replace('\'', '\"')
	pc.copy(standard_block)
	return True, 'block is copied to clipboard' 

def get_address():
#	WHEN PACKAGED WITH PYINSTALLER
	with open(os.path.join(sys._MEIPASS, 'address.txt'), 'r') as f:
		address = f.read()

#	NORMAL USE
#	with open('address.txt', 'r') as f:
#		address = f.read()
	pc.copy(address)
	return f'{address} is copied to clipboard'

def auth(password): ##################################################################
	key = set_cipher(password)[1]	
#	WHEN PACKAGED WITH PYINSTALLER
	with open(os.path.join(sys._MEIPASS, 'encrypted.txt'), 'rb') as f:
		stored_key = f.read()
#	NORMAL USE
#	with open('encrypted.txt', 'rb') as f:
#		stored_key = f.read()
	if stored_key == key: return True
	else: return False
