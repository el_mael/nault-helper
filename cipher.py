from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
import os, sys


def set_cipher(password):
	global decryptor
	global encryptor

#	USE WHEN PACKAGED WITH PYINSTALLER
	try:
		with open(os.path.join(sys._MEIPASS, 'salt.txt'), 'rb') as salt_file:
			salt = salt_file.read() 
	except:
		print('salt does not exist...')
		return

#	NORMAL USE
#	try:
#		with open('salt.txt','rb') as f:
#			salt = f.read() 
#	except:
#		return 'salt does not exist'
#
#	SET CIPHER	
	kdf = PBKDF2HMAC(
		algorithm=hashes.SHA256(),
		length=16+32,
		salt=salt, 
		iterations=480000
	)
	derived = kdf.derive(bytes(password, 'utf-8'))
	iv = derived[:16]
	key = derived[16:]
	cipher = Cipher(algorithms.AES(key), modes.CBC(iv))

# SETUP ENCRYPTOR AND DECRYPTOR
	encryptor = cipher.encryptor()
	decryptor = cipher.decryptor()
	return iv, key


def encrypt(password=None, text=None):
	set_cipher(password)	
	priv_rem=len(text)%16
	text=text+'1'*(16-priv_rem)
	print(len(text))
	priv = bytes(text, 'utf-8')
	ct = encryptor.update(priv) + encryptor.finalize()

#	USE WHEN PACKAGED WITH PYINSTALLER
	with open(os.path.join(sys._MEIPASS, 'encrypted.txt'), 'wb') as f:
		f.write(ct)


#	NORMAL USE
#	with open('encrypted.txt', 'wb') as f:
#		f.write(ct)

	return 'private key is encrypted'


def decrypt(password=None):
	set_cipher(password)

#	USE WHEN PACKAGED WITH PYINSTALLER
	with open(os.path.join(sys._MEIPASS, 'encrypted.txt'),'rb') as f:
		enc = f.read()

#	NORMAL USE
#	with open('encrypted.txt', 'rb') as f:
#		enc = f.read()
	try:
		dt = decryptor.update(enc) + decryptor.finalize()
		dec = dt.decode('utf-8')
	except:
		return 'failed to decrypt' 
	return dec 
